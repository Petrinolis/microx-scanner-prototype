import { useRef, useEffect, useState } from "react";

const DemoAnimation = () => {
  const canvasRef = useRef(null);

  const [ctx, setCtx] = useState(null);
  const [image, setImage] = useState(null);
  const count = useRef(0);

  const width = 600;
  const height = 350;

  const draw = (ctx) => {
    ctx.clearRect(0, 0, width, height);

    ctx.fillStyle = "green";
    ctx.fillRect(0, 0, width, height);
    ctx.drawImage(image, 150 + count.current, 150, 80, 80);
  };

  useEffect(() => {
    const canvas = canvasRef.current;
    const context = canvas.getContext("2d");

    canvas.width = width * 2;
    canvas.height = height * 2;

    canvas.style.width = width + "px";
    canvas.style.height = height + "px";

    context.scale(2, 2);

    const img = new Image();
    img.src = "/luggage.png";
    img.onload = () => {
      setImage(img);
    };

    setCtx(context);
  }, []);

  useEffect(() => {
    if (image !== null) {
      const interval = setInterval(() => {
        if (count.current < 150) {
          count.current = count.current + 3;
        } else {
          count.current = 0;
        }
        draw(ctx);
      }, 20);
      return () => clearInterval(interval);
    }
  }, [image, ctx]);

  return <canvas ref={canvasRef}></canvas>;
};

export default DemoAnimation;
