import styles from "./Panel.module.scss";

const Panel = (props) => {
  const { header, main, footer } = props;
  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>{header}</div>
      <div className={styles.main}>{main}</div>
      <div className={styles.footer}>{footer}</div>
    </div>
  );
};

export default Panel;
