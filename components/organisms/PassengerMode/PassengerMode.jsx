import PropTypes from "prop-types";

import Button from "../../atoms/Button";
import Panel from "../../molecules/Panel";
import DemoAnimation from "../../molecules/DemoAnimation/DemoAnimation";

import { usePassengerMode } from "./usePassengerMode";

const PassengerMode = (props) => {
  const { screen, setScreen } = usePassengerMode(props);

  return (
    <div>
      {screen === "welcome" && (
        <Panel
          header={<h1>Welcome</h1>}
          footer={<Button handleClick={() => setScreen("demo")}>Next</Button>}
        ></Panel>
      )}
      {screen === "demo" && (
        <Panel
          header={<h1>Demo</h1>}
          main={<DemoAnimation />}
          footer={
            <Button handleClick={() => setScreen("bagCheck")}>Next</Button>
          }
        ></Panel>
      )}
      {screen === "bagCheck" && (
        <Panel
          header={<h1>Please Confirm</h1>}
          main={<h1>Are all you item in the locker?</h1>}
          footer={
            <Button
              buttonType={"secondary"}
              handleClick={() => setScreen("scaning")}
            >
              Next
            </Button>
          }
        ></Panel>
      )}
      {screen === "scaning" && (
        <Panel
          header={<h1>Scaning</h1>}
          main={<h1>Please wait</h1>}
          footer={
            <Button handleClick={() => setScreen("welcome")}>Next</Button>
          }
        ></Panel>
      )}
    </div>
  );
};

PassengerMode.defaultProps = {
  initialScreen: "welcome",
};

PassengerMode.propTypes = {
  initialScreen: PropTypes.string,
};

export default PassengerMode;
