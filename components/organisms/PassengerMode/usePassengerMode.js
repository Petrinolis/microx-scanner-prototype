import { useState } from "react";

export const usePassengerMode = (props) => {
  const { initialScreen } = props;

  const [screen, setScreen] = useState(initialScreen);

  return {
    ...props,
    screen,
    setScreen,
  };
};
