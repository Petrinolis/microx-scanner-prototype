import Proptypes from "prop-types";

import styles from "./Button.module.scss";

const Button = (props) => {
  const { buttonType, handleClick, disabled, children } = props;

  return (
    <button
      className={styles[`button--${buttonType}`]}
      onClick={handleClick}
      disabled={disabled}
    >
      {children}
    </button>
  );
};

Button.defaultProps = {
  buttonType: "primary",
  handleClick: () => void 0,
  disabled: false,
};

Button.protoTypes = {
  buttonType: Proptypes.string,
  handleClick: Proptypes.func,
  disabled: Proptypes.bool,
};

export default Button;
