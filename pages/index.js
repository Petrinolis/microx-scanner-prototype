import Head from "next/head";

import PassengerMode from "../components/organisms/PassengerMode";

export default function Home() {
  return (
    <div>
      <Head>
        <title>Microx Scanner Prototype</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <PassengerMode />
      </main>

      <footer></footer>
    </div>
  );
}
